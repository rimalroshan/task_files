## Note: I have used markdown to format this file so I had to add the ".md" extension
## Algorithm:

```
1. Calculate the Square root of the number
    1.1. If the root is in decimal then round it to the next highest integer.
    1.2  Start with this integer.
2. Check if this integer divides the number and is prime.
    2.1. If yes then stop the process and the current integer is the highest prime factor.
    2.2. If no then decrement the integer by 1.
3. Goto step 2      	
```

### Explanation of the Algorithm:

The algorithm starts with the square root of the number if it is a perfect square number or the square root of the number plus one if it is not a perfect square number. Why? Because the highest factor a number can have is either square root of the number or one more than it as mentioned above.

We start with this number and check if it divides the given number and is prime also and whenver we get this number we stop the process and display the number.

Program Termination: 

The program is guaranteed to terminate since we start from sqrt(X) or sqrt(X)+1 and go on decreasing one until we reach 1 after which the program completes regardless of whether such number exists or not.

Exceptions:

- We haven't considered the case for negative number and zero input.
- If the input it self is prime then there doesn't exist any such number so we don't print anything.